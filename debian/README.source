Packaging new upstream versions
===============================

The Debian packaging lives in the master branch, while the upstream development
is pulled in the upstream branch from the upstream git repository. The upstream
source tree has libmseed as a vendored library, and we want to remove it. We do
it as follows:

1. A new upstream version is released, say v2.3.
2. Merge the v2.3 tag in the upstream branch.
3. Branch the upstream branch to a new branch called v2.3+ds.
4. Remove the vendored library (git rm -r libmseed) and commit.
5. Tag this as v2.3+ds1.
6. Checkout master and prepare the Debian packave with version 2.3+ds1-1.
7. Build, test/lint, upload, tag, push, as usual.
8. Remember to push the v2.3+ds branch. This is not done automatically by
   gbp push as it considers 'upstream' to be the only upstream branch.
